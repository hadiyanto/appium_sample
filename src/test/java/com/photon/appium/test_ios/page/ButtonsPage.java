package com.photon.appium.test_ios.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/** Page object for the buttons page **/
public class ButtonsPage extends AbstractPage{

	@FindBy(xpath = "//UIAStaticText[@name='Background Image']")
//	@FindBy(xpath = "//UIAApplication[1]/UIAWindow[2]/UIATableView[1]/UIATableCell[1]/UIAStaticText[@name='Background Image']")
	public WebElement backgroundImageText;
	
	@FindBy(name = "Gray")
	public WebElement grayBtn;
	
}
