package com.photon.appium.test_ios.page;

import io.appium.java_client.pagefactory.iOSFindBy;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends AbstractPage{

	@iOSFindBy(name = "Buttons")
	public WebElement buttonsBtn;

	@FindBy(name = "Controls")
	public WebElement controlsBtn;
	
	@FindBy(name = "TextFields")
	public WebElement textFieldsBtn;
	
}
