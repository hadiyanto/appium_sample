package com.photon.appium.test_ios.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public abstract class AbstractPage {
	
	@FindBy(name = "Back")
	public WebElement backBtn;
}
