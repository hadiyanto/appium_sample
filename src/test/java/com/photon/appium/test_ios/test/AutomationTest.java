package com.photon.appium.test_ios.test;

import org.junit.Assert;
import org.junit.Test;

import com.photon.appium.test_ios.common.BaseAcceptanceTest;

public class AutomationTest extends BaseAcceptanceTest{

	@Test
	public void one() throws Exception {
		System.out.println("Click Buttons button on home page");
		homePage.buttonsBtn.click();
		
		System.out.println("Check Backgroud Image is displayed");
		String expected = "Background Image";
		String actual = buttonPage.backgroundImageText.getText();
		
		// Example for assertion
		Assert.assertTrue(buttonPage.backgroundImageText.isDisplayed());
		Assert.assertEquals("Info : ",expected, actual);
		
		System.out.println("Check Gray Button is displayed");
		buttonPage.grayBtn.isDisplayed();
		
		System.out.println("Click back button on header");
		homePage.backBtn.click();
		
		System.out.println("Click Controls button on home page");
		homePage.controlsBtn.click();
		
		System.out.println("Click back button on header");
		homePage.backBtn.click();
		
		System.out.println("Click text fields button on header");
		homePage.textFieldsBtn.click();

	}
	
}
