package com.photon.appium.test_ios.common;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.remote.MobileCapabilityType;

import java.io.File;
import java.net.URL;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;

import com.photon.appium.test_ios.page.AlertPage;
import com.photon.appium.test_ios.page.ButtonsPage;
import com.photon.appium.test_ios.page.ControlsPage;
import com.photon.appium.test_ios.page.HomePage;
import com.photon.appium.test_ios.page.PickerPage;
import com.photon.appium.test_ios.page.TextFieldsPage;

public class BaseAcceptanceTest {

	protected WebDriver driver;
	protected HomePage homePage;
	protected ButtonsPage buttonPage;
	protected AlertPage alertPage;
	protected ControlsPage controlsPage;
	protected PickerPage pickerPage;
	protected TextFieldsPage textFieldsPage;

	/**
	 * Run before each test *
	 */
	@Before
	public void setUp() throws Exception {
		String userDir = System.getProperty("user.dir");
		String localApp = "UICatalog.app";
		File app = new File(userDir, localApp);
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");
		capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "8.1");
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 6");
		capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());

		driver = new IOSDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		setupPages();
	}

	/**
	 * Run after each test *
	 */
	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	public void setupPages() {
		homePage = new HomePage();
		buttonPage = new ButtonsPage();
		alertPage = new AlertPage();
		controlsPage = new ControlsPage();
		pickerPage = new PickerPage();
		textFieldsPage = new TextFieldsPage();
		PageFactory.initElements(new AppiumFieldDecorator(driver), homePage);
		PageFactory.initElements(new AppiumFieldDecorator(driver), buttonPage);
		PageFactory.initElements(new AppiumFieldDecorator(driver), alertPage);
		PageFactory.initElements(new AppiumFieldDecorator(driver), controlsPage);
		PageFactory.initElements(new AppiumFieldDecorator(driver), pickerPage);
		PageFactory.initElements(new AppiumFieldDecorator(driver), textFieldsPage);
	}
}