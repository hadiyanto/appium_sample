Sample Appium JUnit project
---

This contains the source code for running sample [Appium]( https://github.com/Hadiyanto/appium_sample.git)
tests using [JUnit](http://www.junit.org).

In order to run the tests, you will need to install [Apache Maven](http://maven.apache.org),
and Appium (according to the Appium [installation instructions](https://github.com/appium/appium).

You will then need to start appium, eg:

    appium --session-override

To compile and run all tests, run:

    mvn clean test

To run a single test, run:

    mvn -Dtest=com.photon.appium.test_ios.test.AutomationTest.java test
    